# Cypress CI CD example

Researching integrate cypress test with CI/CD

## Resources
### From Cypress documentation:

- Continuous integration -> [Introduction](https://docs.cypress.io/guides/continuous-integration/introduction)
- Continuous Integration -> [GitLab CI](https://docs.cypress.io/guides/continuous-integration/gitlab-ci)
- [Docker images](https://docs.cypress.io/examples/examples/docker) for running Cypress locally and in CI.
- Cypress [example](https://gitlab.com/cypress-io/cypress-example-docker-gitlab) repository on CitLab.
- Command Line [options](https://docs.cypress.io/guides/guides/command-line#cypress-run) ```cypress run [options]```

### Command examples
- ```cypress run --browser chrome``` run on specific browser (default Electron)
- ```cypress run --spec "cypress/integration/one/test_success_1.cy.ts"``` run specific test
- ```cypress run --browser chrome --env url=http://localhost:3000/``` pass base url during --env, overwriting env variable from cypress.json. Using in test: ```cy.visit(Cypress.env('url'))```.
- ```cypress run --browser chrome --env url=${URL_FOR_TEST}```
- ```cypress run --record --key <record_key>"``` record_key from cypress dashboard
- ```cypress run --browser chrome --record --key ${PROJECT_RECORD_KEY} --env url=${URL_FOR_TEST}```
- ```cypress run --config "viewportWidth=375,viewportHeight=667"``` for testing on mobile

### Cypress Dashboard
- My [dashboard](https://dashboard.cypress.io/organizations/14e4568b-85e5-4d64-8c29-2b58edec1afa/projects) to this project
- Billing & Usage: list of [billing plans](https://dashboard.cypress.io/organizations/14e4568b-85e5-4d64-8c29-2b58edec1afa/pricing)
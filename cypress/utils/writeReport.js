const moment = require("moment/moment");

function writeReport() {

    const ci_commit_sha_value = process.env.CI_COMMIT_SHA;
    const ci_pipeline_id_value = process.env.CI_PIPELINE_ID;
    const ci_job_id_value = process.env.CI_JOB_ID;
    const ci_job_started_at_value = moment(new Date(process.env.CI_JOB_STARTED_AT)).format('YYYY-MM-DD HH:mm:ssZ').slice(0, -3);
    const screenshots_path_value = process.env.CI_JOB_URL + `/artifacts/browse/screenshots`;
    const url = process.env.URL_FOR_TEST;

    console.log(`CI_COMMIT_SHA: ${ci_commit_sha_value}`);
    console.log(`CI_PIPELINE_ID: ${ci_pipeline_id_value}`);
    console.log(`CI_JOB_ID: ${ci_job_id_value}`);
    console.log(`CI_JOB_STARTED_AT: ${ci_job_started_at_value}`);
    console.log(`screenshots_path: ${screenshots_path_value}`);
    console.log(`url: ${url}`);

}

writeReport();
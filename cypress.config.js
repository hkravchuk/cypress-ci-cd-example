const {defineConfig} = require("cypress");
const {exec} = require('child_process');

module.exports = defineConfig({
    chromeWebSecurity: false,
    pageLoadTimeout: 15000,
    viewportHeight: 1000,
    viewportWidth: 1600,
    video: false,
    reporter: 'mochawesome',
    reporterOptions: {
        reportDir: 'cypress/reports',
        overwrite: false,
        html: false,
        json: true,
    },
    env: {
        url: "http://localhost:3000/",
    },
    e2e: {
        setupNodeEvents(on, config) {
            on('before:run', () => {
                console.log('before:run. Make directory ./cypress/reports');
                exec('mkdir -p ./cypress/reports');
                exec('mkdir -p ./reports');
            })
            on('before:browser:launch', (browser = {}, launchOptions) => {
                console.log(
                    'before:browser:launch. Is launching browser %s headless? %s',
                    browser.name,
                    browser.isHeadless,
                );
                const width = 1920;
                const height = 1080;
                console.log('before:browser:launch. Setting the browser window size to %d x %d.', width, height);

                if (browser.name === 'chrome') {
                    launchOptions.args.push(`--window-size=${width},${height}`);
                    // force screen to be non-retina and just use our given resolution
                    launchOptions.args.push('--force-device-scale-factor=1');
                }
                return launchOptions;
            })
            on('after:run', () => {
                console.log('after:run. Move reports');
                exec(`cp -r ./cypress/reports ./reports/${process.env.CI_JOB_ID} `);
            })
        },
        specPattern: 'cypress/integration/**/*.cy.{js,jsx,ts,tsx}',
        supportFile: 'cypress/support/index.js',
    },
});
